# Translation of cdimage.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2013-05-14 13:10+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "      Key fingerprint"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr "Obrazy ISO"

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr "Súbory Jigdo"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />FAQ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Stiahnuť prostredníctvom Jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Stiahnuť prostredníctvom HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Kúpiť CD/DVD"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Sieťová inštalácia"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Stiahnuť"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Rôzne"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Výtvarné diela"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Zrkadlá"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Zrkadlá rsync"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />Overiť"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Stiahnuť torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_relinfo\" />Image Release Info"
msgstr "<void id=\"dc_relinfo\" />Info o vydaní obrazov"

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Tím Debian CD"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr "debian_na_cd"

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />faq"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr "http_ftp"

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "kúpiť"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr "sieťová_inštalácia"

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />rôzne"

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"<a href=\"/MailingLists/disclaimer\">Verejná konferencia</a> o CD/DVD (v "
"angličtine):"
